from django.apps import AppConfig


class RespyreConfig(AppConfig):
    name = 'respyre'
