from django.contrib import admin
from respyre.models import AudioUploadDetail


class AudioUploadDetailAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'time_created','audio','analytics_data')
    actions = None

admin.site.register(AudioUploadDetail, AudioUploadDetailAdmin)

# Register your models here.
