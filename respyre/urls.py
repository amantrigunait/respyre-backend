from django.conf.urls import url

from respyre import views

urlpatterns = [
#    url(r'^audio_analysis/$', views.audio_analysis, name='audio analysis'),
      url(r'^$', views.index, name='index'),
      url(r'^audio_analysis/$', views.audio_analysis, name='audio_analysis'),
      url(r'^audio_uploads/$', views.audio_uploads, name='audio_uploads'),
      url(r'^csv_download/(?P<csvfile>\w{0,50})/$',views.csv_download,name='csv_url'),
]
