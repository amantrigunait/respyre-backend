# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2020-09-17 00:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AudioUploadDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, default='', max_length=32, verbose_name='Name')),
                ('email', models.CharField(blank=True, default='NA', max_length=64, null=True, verbose_name='Email')),
                ('audio', models.CharField(blank=True, default='NA', max_length=128, null=True, verbose_name='Audio File')),
                ('analytics_data', models.CharField(blank=True, default='NA', max_length=128, null=True, verbose_name='Analytics Data')),
                ('time_created', models.DateTimeField(auto_now_add=True, verbose_name='Uploaded on')),
            ],
            options={
                'verbose_name_plural': 'Audio Data',
            },
        ),
    ]
