from django.http import HttpResponse
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
import numpy as np
import pandas as pd
from scipy import signal
from pyAudioAnalysis import ShortTermFeatures as sf
#import librosa
from scipy.io import wavfile
from scipy.signal.filter_design import butter
from scipy.signal import lfilter, hilbert, chirp,find_peaks,filtfilt, find_peaks_cwt, spectrogram
from respyre.forms import UpdateAudioDetailsForm
import time
import copy
from respyre.models import AudioUploadDetail
from pydub import AudioSegment

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def sine_generator(fs, sinefreq, duration):
    T = duration
    nsamples = fs * T
    w = 2. * np.pi * sinefreq
    t_sine = np.linspace(0, T, nsamples, endpoint=False)
    y_sine = np.sin(w * t_sine)
    result = pd.DataFrame({ 
        'data' : y_sine} ,index=t_sine)
    return result

def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = signal.butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y

def save_audio_file(audio,filename):
    myfile = audio
    fd = open('/home/ubuntu/respyre/respyre-backend/audio_uploads/' + filename, 'wb+', 0o777)
    for chunk in myfile.chunks():
        fd.write(chunk)
    fd.close()
    return True


def audio_analysis(request):
  message = ''
  if request.method == 'POST':
    form = UpdateAudioDetailsForm(request.POST, request.FILES)
    if form.is_valid():
      print("Audio Analysis")
      name = form.cleaned_data['name']
      name = name.replace(' ','')
      email = form.cleaned_data['email']
      exercise = form.cleaned_data['exercise']
      audio_file = request.FILES['audio_file']
      if audio_file.multiple_chunks():
        print("File Size Large")
      if('.wav' not in audio_file.name):
          message = 'Only wav format audio file are permissible'
          return render(request,'audio_upload.html', {'form': form,'message':message}) 
      #audio_file_tmp = copy.deepcopy(audio_file)
      time_millis = time.time()
      filename = str(name)+'_'+str(exercise)+'_'+str(time_millis)+'.wav'
      audio_name = name
      audio_email= email
      audio_audio= "/home/ubuntu/respyre/respyre-backend/audio_uploads/" + filename
      audio_analytics_data="none"

      print(filename, " Filename")
      #save_audio_file(audio_file_tmp,filename)
      print("Name :", name, " Email :", email, " Exercise :", exercise)
      audio_file_2 = AudioSegment.from_wav(audio_file)
      audio_file_2= audio_file_2.set_channels(1)
      print(audio_file_2)
      audio_file_2.export("/home/ubuntu/respyre/respyre-backend/audio_uploads/"+filename, format="wav")

      print("RUNING AUDIO ANALYSIS")
      fs_wav, data_wav = wavfile.read("/home/ubuntu/respyre/respyre-backend/audio_uploads/"+filename)
      #data_wav, fs_wav = librosa.load("/home/ubuntu/Audio_004-21.wav", sr=8000, mono=True, offset=0.0)
      print(fs_wav)

      print('Signal Duration = {} seconds'.
            format(data_wav.shape[0] / fs_wav))

      duration = (data_wav.shape[0] / fs_wav)
      print(duration, " Duration")
      count_minutes = int(duration/60)
      print(count_minutes, " minutes")

      window_len= 0.1 #in second
      window_steps = 0.1 #in seconds
      #data_wav = data_wav[(1)*60 * fs_wav: (2)*60 * fs_wav]

      for i in range(0,count_minutes):
        data_wav_tmp = data_wav[(i)*60 * fs_wav: (i+1)*60 * fs_wav]
        data_wav_high = butter_highpass_filter(data_wav_tmp, 200, fs_wav, order=3)

        b, a = butter(4, 1000/(fs_wav*0.5), btype = 'low', analog=False)
        filtered = lfilter(b, a, data_wav_high)
        data_wav_lp = filtered
        print(len(data_wav_lp), "Length")
        data_wav_lp = data_wav_lp/ (2**15)



        #F, f_names = sf.feature_extraction(data_wav, fs_wav, 0.100*fs_wav, 0.050*fs_wav)
        r,t,f=sf.spectrogram(data_wav_lp,fs_wav,0.100*fs_wav,0.100*fs_wav,plot=False)

        r_shape = r.shape
        print(r_shape[0])
        print(t)

        sum_array = []
        for i in range(1,r_shape[0]+1):
          sum_tmp = np.sum(r[i-1])
          sum_array.append(sum_tmp)
          # print(" row ", sum_tmp)
        print(np.mean(sum_array))
        print(np.std(sum_array))
        mean_sum_array = np.mean(sum_array)
        std_sum_array = np.std(sum_array)
        mean_threshold_change_count = 0
        length_sum_array = len(sum_array)
        idx_low=[]
        idx_high=[]
        delta = mean_sum_array
        last_threshold=0

        for s_indx, s in enumerate(sum_array):
          if s<=mean_sum_array-(std_sum_array/2) and last_threshold is not "low":
            if last_threshold == 0:
              idx_low.append([0,sum_array[0]])
            else:
              idx_low.append([s_indx,s])
            last_threshold="low"
          elif s>=mean_sum_array+(std_sum_array/2) and last_threshold is not "high":
            if last_threshold == 0:
              idx_high.append([0,sum_array[0]])
            else:
              idx_high.append([s_indx,s])
            last_threshold="high"

        print(len(idx_high)," idx_high length")
        print(len(idx_low), " idx_low length")

        last_breath_exhale = False
        last_breath_inhale = False

        if(len(idx_low)<len(idx_high)):
          last_breath_exhale = True
        if(len(idx_high)== len(idx_low)) and (idx_high[-1][0]>idx_low[-1][0]):
          last_breath_exhale = True
        if(len(idx_high)== len(idx_low)) and (idx_high[-1][0]<idx_low[-1][0]):
          last_breath_inhale = True
        if(len(idx_low)>len(idx_high)):
          last_breath_inhale = True
        print("idx high", idx_high)
        index_exhalations = [] #array [[start,end]]
        index_inhalations = [] #array [[start,end]]
        #Indexing Exhale Start and End Index
        for idx in range(0,len(idx_high)):
          if (idx_high[0][0]==0):
            if idx==(len(idx_high)-1) and last_breath_exhale:
              index_exhalations.append([idx_high[idx][0],length_sum_array-1])
              break
            else:
              index_inhalations.append([idx_high[idx][0],idx_low[idx][0]])
          else:
            if idx==(len(idx_high)-1) and last_breath_exhale:
              index_exhalations.append([idx_high[idx][0],length_sum_array-1])
              break
            else:
              index_exhalations.append([idx_high[idx][0],idx_low[idx+1][0]])
        #Indexing Inhale Start and End Index
        for idx in range(0,len(idx_low)):
            if (idx_low[0][0]==0):
              if idx==(len(idx_low)-1) and last_breath_inhale:
                index_inhalations.append([idx_low[idx][0],length_sum_array-1])
                break
              else:
                index_inhalations.append([idx_low[idx][0],idx_high[idx][0]])
            else:
              if idx==(len(idx_low)-1) and last_breath_inhale:
                index_inhalations.append([idx_low[idx][0],length_sum_array-1])
                break
              else:
                index_inhalations.append([idx_low[idx][0],idx_high[idx+1][0]])

        time_exhale=(np.array(index_exhalations))* window_steps
        time_inhale=(np.array(index_inhalations))* window_steps
        f = open('/home/ubuntu/respyre/respyre-backend/csv/'+name+'_'+str(time_millis).replace('.', '')+'.csv','a')
        for idx in range(0,len(time_exhale)):
            try:
              csv_data = str(time_exhale[idx][0])+","+str(time_exhale[idx][1])+","+str(time_inhale[idx][0])+","+str(time_inhale[idx][1])
              f.write(csv_data)
              f.write('\n')
            except:
                pass
        f.close()

        print("Time Exhale", time_exhale.tolist())
        print("Time Inhale", time_inhale.tolist())
      
#      f = open('/home/ubuntu/respyre/respyre-backend/csv/'+name+'_'+str(time_millis).replace('.', '')+'.txt','w')
#      csv_data_1= str("Exhale Time")+ " "+ str(time_exhale.tolist())
#      csv_data_2= str("Inhale Time")+" "+str(time_inhale.tolist())
#      csv_data_3= str("Duration")+" "+str(duration)
#      f.write(csv_data_1) #Give your csv text here.
#      f.write('\n')
#      f.write('\n')
#      f.write('\n')
#      f.write(csv_data_2) #Give your csv text here.
#      f.write('\n')
#      f.write('\n')
#      f.write('\n')
#      f.write(csv_data_3) #Give your csv text here.
#      f.write('\n')
#      f.write('\n')
#      f.write('\n')
#      f.close()     
      message = "File Uploaded Successfully, You shall be notified of the analysis over email."
      audio_analytics_data = 'http://ec2-3-14-11-138.us-east-2.compute.amazonaws.com/analysis/csv_download/'+name+'_'+str(time_millis).replace('.', '')
      AudioUploadDetail(name=audio_name,email=audio_email,audio=audio_audio,analytics_data=audio_analytics_data).save()
    
  else:
    form = UpdateAudioDetailsForm()
  return render(request,'audio_upload.html', {'form': form,'message':message})


def csv_download(request,csvfile):
  csvfile_name = str(csvfile)
  csvfile =  open('/home/ubuntu/respyre/respyre-backend/csv/'+csvfile_name+'.csv', 'r')
  response = HttpResponse(csvfile, content_type='text/csv')
  response['Content-Disposition'] = 'attachment; filename="%s"' % csvfile_name + '.csv'
  return response


def audio_uploads(request):
    url_path = "/admin/audiouploaddetail/"
    return HttpResponseRedirect(url_path)
