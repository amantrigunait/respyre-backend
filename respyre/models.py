from django.db import models

class VerificationManager(models.Manager):
    def get_queryset(self):
        return super(VerificationManager, self).get_queryset()

class AudioUploadDetail(models.Model):
    name = models.CharField(verbose_name="Name", max_length=32, default="", db_index=True)
    email = models.CharField(verbose_name="Email", max_length=64, null=True, blank=True, default="NA")
    audio =  models.CharField(verbose_name="Audio File", max_length=128, null=True, blank=True, default="NA")
    analytics_data =  models.CharField(verbose_name="Analytics Data", max_length=128, null=True, blank=True, default="NA")
    time_created = models.DateTimeField(verbose_name="Uploaded on", blank=True, auto_now_add=True)

    objects = VerificationManager()

    class Meta:
        verbose_name_plural = 'Audio Data'

    def __unicode__(self):
        return self.name

# Create your models here.
