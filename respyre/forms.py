from django import forms
from django.forms import ModelForm

EXERCISE_CHOICE = ((1, "Running"), (2, "Yoga"), (3, "Heavy Workout"),(4, "Jumba") )

class UpdateAudioDetailsForm(forms.Form):
    name = forms.CharField(label='Name', max_length=50, required=True, widget=forms.TextInput(attrs={'placeholder': 'max length: 50'}))
    email = forms.EmailField(label='Email', max_length=50, required=True, widget=forms.TextInput(attrs={'placeholder': 'max length: 50'}))
    exercise = forms.ChoiceField(label='Exercise', choices=EXERCISE_CHOICE)
    audio_file = forms.FileField(label='Audio File', required=True)

    def __init__(self, data=None, *args, **kwargs):
        super(UpdateAudioDetailsForm, self).__init__(data, *args, **kwargs)


